﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(GAPSoft.Startup))]
namespace GAPSoft
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
