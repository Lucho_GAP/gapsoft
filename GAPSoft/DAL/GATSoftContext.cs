﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.Entity;
using System.Data.Entity.ModelConfiguration.Conventions;
using GAPSoft.Models;
namespace GAPSoft.DAL
{
    public class GATSoftContext : DbContext
    {
        public GATSoftContext()
            : base("GAPSoftConnection")
        {

        }
        public DbSet<EstadosClientes> EstadosClientes { get; set; }
        public DbSet<Proveedores> Proveedores { get; set; }
        public DbSet<EstadosProductos> EstadosProductos { get; set; }       
        public DbSet<FamiliasProductos> FamiliasProductos { get; set; }
        public DbSet<GruposProductos> GruposProductos { get; set; }
        public DbSet<EstadosIva> EstadosIva { get; set; }
        public DbSet<TiposIva> TiposIva { get; set; }
        public DbSet<EstadosPedidos> EstadosPedidos { get; set; }
        public DbSet<Productos> Productos { get; set; }
        public DbSet<Produccion> Produccion { get; set; }
        public DbSet<EstadosCompras> EstadosCompras { get; set; }
        public DbSet<MateriasPrimas> MateriasPrimas { get; set; }
        public DbSet<Componentes> Componentes { get; set; }
        public DbSet<DetalleComponentes> DetalleComponentes { get; set; }
        public DbSet<TiposPagos> TiposPagos { get; set; }
        public DbSet<Clientes> Clientes { get; set; }
        public DbSet<Facturas> Facturas { get; set; }
        public DbSet<DetalleFacturas> DetalleFacturas { get; set; }
        public DbSet<PuntosClientes> PuntosClientes { get; set; }
        public DbSet<ReglaFidelizacion> ReglaFidelizacion { get; set; }
        public DbSet<DetallePuntos> DetallePuntos { get; set; }
        public DbSet<PedidosClientes> PedidosClientes { get; set; }
        public DbSet<DetallePedidos> DetallePedidos { get; set; }
        public DbSet<Compras> Compras { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Conventions.Remove<PluralizingTableNameConvention>();
            modelBuilder.Conventions.Remove<OneToManyCascadeDeleteConvention>();
        }

    }
}