﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.Entity;
using System.Collections.Generic;
using GAPSoft.Models;
using GAPSoft.DAL;
namespace GAPSoft.DAL
{
    public class GAPSoftInitializer : System.Data.Entity.DropCreateDatabaseIfModelChanges<GATSoftContext>
    {
        
        protected override void Seed(GATSoftContext context)
        {
            //inicialización de EstadoS De proveedores
            var estadosClientes = new List<EstadosClientes>
            {
                new EstadosClientes{ID=1,Descripcion="Activo"},
                new EstadosClientes{ID=2,Descripcion="Inactivo"},
                new EstadosClientes{ID=3,Descripcion= "Aplazado"}
            };
            estadosClientes.ForEach(estado => context.EstadosClientes.Add(estado));
            context.SaveChanges();

            //Inicialización de proveedores
            var proveedores = new List<Proveedores>
           {
            new Proveedores{ID=1, Descripcion="Exito S.A.", Email="exito@exito.com", Contacto="Miguel Areas", EstadosClientesID=1, Telefono="123456"},
            new Proveedores{ID=2, Descripcion="TODO PAN", Email="pan@hotmail.com", Contacto="Adriana Mendez", EstadosClientesID=1, Telefono="3148765786"},
            new Proveedores{ID=3, Descripcion="Gradis PAN S.A.", Email="grasa@pan.com", Contacto="Jose Perez", EstadosClientesID=1, Telefono="8765489"},
            new Proveedores{ID=4, Descripcion="JoseRrago.", Email="ventas@joserrago.com.co", Contacto="Ana Maria Garcia", EstadosClientesID=1, Telefono="2436087"},
            new Proveedores{ID=5, Descripcion="Cimpa S.A.S", Email="cimpa@cimpa.com.co", Contacto="Mario Sabogal", EstadosClientesID=1, Telefono="4203098"},
            new Proveedores{ID=6, Descripcion="Enzipan S.A.", Email="laboratorio@enzipan.com.co", Contacto="Andrea Ramirez", EstadosClientesID=1, Telefono="7423567"},
            new Proveedores{ID=7, Descripcion="HARINERA INDUPAN S.A.S.", Email="indupan@indupan.com.co", Contacto="Carlos Trujillo", EstadosClientesID=1, Telefono="8765489"},
            new Proveedores{ID=8, Descripcion="Sigra S.A.", Email="servicio.maestro@sigra.com", Contacto="Fernando Alarcon", EstadosClientesID=1, Telefono="5194568"},
            new Proveedores{ID=9, Descripcion="Nacional Coffee S.A.S", Email="nacional@coffee.com.co", Contacto="Nancy Aguilar ", EstadosClientesID=1, Telefono="3598760"},
            new Proveedores{ID=10, Descripcion="Quesera San Juan S.A.", Email="quesera@sanjuan.com.co", Contacto="Jorge Castillo", EstadosClientesID=1, Telefono="7456890"},
            new Proveedores{ID=11, Descripcion="COLDANZIMAS LTDA.", Email="coldan@coldanzimasltda.com.co", Contacto="Jose Perez", EstadosClientesID=1, Telefono="3108890"}
           };
            proveedores.ForEach(prove => context.Proveedores.Add(prove));
            context.SaveChanges();

            //inicialización de los estados Producto
            var estadoProductos= new List<EstadosProductos>
            {
                new EstadosProductos{ID=1, Descripcion="Activo"},
                new EstadosProductos{ID=2, Descripcion="Inactivo"}
            };
            estadoProductos.ForEach(estadoProduc=>context.EstadosProductos.Add(estadoProduc));
            context.SaveChanges();

            //inicialización de Familias de producto
            var familias = new List<FamiliasProductos>
            {
                new FamiliasProductos{ID=1,Descripcion="Productos de Pandería", EstadosProductoID=1},
                new FamiliasProductos{ID=2,Descripcion="Bebidas",EstadosProductoID=1},
                new FamiliasProductos{ID=3,Descripcion="Productos de pastelería y repostería",EstadosProductoID=1},
                new FamiliasProductos{ID=4,Descripcion="Lácteos y Huevos",EstadosProductoID=1},
                new FamiliasProductos{ID=5,Descripcion="Productos de Confíteria",EstadosProductoID=1}
            };
            familias.ForEach(fami=> context.FamiliasProductos.Add(fami));
            context.SaveChanges();

            //inicializacion de gropos productos
            var grupos=  new  List<GruposProductos>
            {
                new GruposProductos{ID=1, Descripcion= "Pan y galletas",FamiliasProductosID=1, EstadosProductosID=1},
                new GruposProductos{ID=2,Descripcion="Bebidas Calientes", FamiliasProductosID=2, EstadosProductosID=1},
                new GruposProductos{ID=3,Descripcion="Bebidas Frías", FamiliasProductosID=2, EstadosProductosID=1},
                new GruposProductos{ID=4,Descripcion="Huevos y sustitutos", FamiliasProductosID=4, EstadosProductosID=1},
                new GruposProductos{ID=5,Descripcion="Queso", FamiliasProductosID=4, EstadosProductosID=1},
                new GruposProductos{ID=6,Descripcion="pastelería y repostería dulce", FamiliasProductosID=4, EstadosProductosID=1},
                new GruposProductos{ID=7,Descripcion="pastelería y repostería salada", FamiliasProductosID=4, EstadosProductosID=1},

            };  
            grupos.ForEach(gru=> context.GruposProductos.Add(gru));
            context.SaveChanges();

            //inicializacion de estados de iva
            var estadosIva = new List<EstadosIva>
            {
               new EstadosIva{ ID=1, Descripcion="Activo"},
               new EstadosIva{ ID=2, Descripcion="Inactivo"}
            };
            estadosIva.ForEach(estadoIva => context.EstadosIva.Add(estadoIva));
            context.SaveChanges();

            //inicializacion de tipos de iva
            var iva = new List<TiposIva>
            {
               new TiposIva{ID=1,Iva=10, Descripcion="iva del 16%", EstadosIvaID= 1},
               new TiposIva{ ID=3, Iva= 10, Descripcion="iva del 10%", EstadosIvaID=1}
            };
            iva.ForEach(tipoIva => context.TiposIva.Add(tipoIva));
            context.SaveChanges();

            //inicializacion de estados de productos
            var estadosPedidos = new List<EstadosPedidos>
            {
               new EstadosPedidos{ ID=1, Descripcion="Activo"},
               new EstadosPedidos{ ID=2, Descripcion="Inactivo"}
            };
            estadosPedidos.ForEach(estadoPedi => context.EstadosPedidos.Add(estadoPedi));
            context.SaveChanges();

            //inicializacion de productos 
            var productos = new List<Productos>
            {
              new  Productos {ID=1, Descripcion="Café",PrecioVenta=800, Stock= 5, StockPedido=1,GruposProductosID=2,EstadosPedidosID=1,TiposIvaID=1 },
              new  Productos {ID=2, Descripcion="Chapuchino",PrecioVenta=1500, Stock= 2, StockPedido=6,GruposProductosID=2,EstadosPedidosID=1,TiposIvaID=1 },
              new Productos {ID=3, Descripcion="Pan Congelado",PrecioVenta=1000,Stock=10,StockPedido=10,GruposProductosID=1,EstadosPedidosID=1,TiposIvaID=1},
              new Productos {ID=4, Descripcion="Pan Frances",PrecioVenta=1000,Stock=10,StockPedido=10,GruposProductosID=1,EstadosPedidosID=1,TiposIvaID=1},
              new Productos {ID=5, Descripcion="Huevos en cascara de gallina",PrecioVenta=250,Stock=120,StockPedido=120,GruposProductosID=4,EstadosPedidosID=1,TiposIvaID=1},
              new Productos {ID=6, Descripcion="Gaseosa Cocacola",PrecioVenta=1500,Stock=90,StockPedido=90,GruposProductosID=3,EstadosPedidosID=1,TiposIvaID=1},
              new Productos {ID=7, Descripcion="Queso Natural",PrecioVenta=3000,Stock=2,StockPedido=3,GruposProductosID=5,EstadosPedidosID=1,TiposIvaID=1},
              new Productos {ID=8, Descripcion="Queso Costeño",PrecioVenta=5000,Stock=3,StockPedido=5,GruposProductosID=5,EstadosPedidosID=1,TiposIvaID=1},
              new Productos {ID=9, Descripcion="Pastel de Pollo",PrecioVenta=1500,Stock=10,StockPedido=100,GruposProductosID=7,EstadosPedidosID=1,TiposIvaID=1},
              new Productos {ID=10, Descripcion="Pastel de carne",PrecioVenta=1500,Stock=3,StockPedido=5,GruposProductosID=7,EstadosPedidosID=1,TiposIvaID=1},
              new Productos {ID=11, Descripcion="Brownie",PrecioVenta=1500,Stock=3,StockPedido=5,GruposProductosID=6,EstadosPedidosID=1,TiposIvaID=1},
              new Productos {ID=12, Descripcion="Brazo de Reina",PrecioVenta=1500,Stock=3,StockPedido=5,GruposProductosID=6,EstadosPedidosID=1,TiposIvaID=1}
            };
            productos.ForEach(product => context.Productos.Add(product));
            context.SaveChanges();

            //inicializacion de tabla Produccion
            var produccion = new List<Produccion>
            {
                new  Produccion{ID= 1, Cantidad=3, Fecha=Convert.ToDateTime("2015-07-01"), ProductosID=2},
                new  Produccion{ID= 4, Cantidad=19, Fecha=Convert.ToDateTime("2015-07-10"), ProductosID=3},
                new  Produccion{ID= 4, Cantidad=19, Fecha=Convert.ToDateTime("2015-07-10"), ProductosID=4},
                new  Produccion{ID= 2, Cantidad=12, Fecha=Convert.ToDateTime("2015-07-02"), ProductosID=2},
                new  Produccion{ID= 3, Cantidad=49, Fecha=Convert.ToDateTime("2015-07-05"), ProductosID=2},
                new  Produccion{ID= 4, Cantidad=19, Fecha=Convert.ToDateTime("2015-07-10"), ProductosID=2}
            };
            produccion.ForEach(prod => context.Produccion.Add(prod));
            context.SaveChanges();

            //inicializacion de estados de productos
            var estadosCompras = new List<EstadosCompras>
            {
               new EstadosCompras{ ID=1, Descripcion="En Proceso"},
               new EstadosCompras{ ID=2, Descripcion="Entregado"},
               new EstadosCompras{ ID=3, Descripcion="Solicitado"}
            };
            estadosCompras.ForEach(estadoComp => context.EstadosCompras.Add(estadoComp));
            context.SaveChanges();

            //inicializacion de Materias Primas
            var materias = new List<MateriasPrimas>
            {
                new MateriasPrimas{ID=1, Cantidad=30, Descripcion="Gaseosa Cocacola", Stock=5, StockCompra=10, PrecioCompra=1000,EstadosComprasID=1},
                new MateriasPrimas{ID=2, Cantidad=46, Descripcion="Harina Haz de Oro", Stock=12, StockCompra=80, PrecioCompra=1000,EstadosComprasID=1},
                new MateriasPrimas{ID=3, Cantidad=90, Descripcion="Huevos de cascara Gallina", Stock=60, StockCompra=420, PrecioCompra=250,EstadosComprasID=2},
                new MateriasPrimas{ID=4, Cantidad=50, Descripcion="Grasa Hojalpan", Stock=10, StockCompra=10, PrecioCompra=5000,EstadosComprasID=3},
                new MateriasPrimas{ID=5, Cantidad=50, Descripcion="Queso Costeño", Stock=4, StockCompra=8, PrecioCompra=5000,EstadosComprasID=1}
            };
            materias.ForEach(mat => context.MateriasPrimas.Add(mat));
            context.SaveChanges();

            //inicializacion de componentes
            var componentes = new List<Componentes>
            {
                new Componentes{ID=1, ProductosID=1},
                new Componentes{ID=2, ProductosID=4}
            };
            componentes.ForEach(compon => context.Componentes.Add(compon));
            context.SaveChanges();

            //inicializacion detallesComponentes
             var dtComponesntes = new List<DetalleComponentes>
            {
                new DetalleComponentes{ID=1, Cantidad=5, ComponentesID= 1, MateriasPrimasID=1},
                new DetalleComponentes{ID=2, Cantidad=1, ComponentesID= 2, MateriasPrimasID=3},
                new DetalleComponentes{ID=3, Cantidad=2, ComponentesID= 2, MateriasPrimasID=2}
            };
            dtComponesntes.ForEach(dtCompo => context.DetalleComponentes.Add(dtCompo));
            context.SaveChanges();

            //inicialización de tiposPagos
            var pagos = new List<TiposPagos>
            {
                new TiposPagos {ID=1, Descripcion="Contado"},
                new TiposPagos{ID=2, Descripcion="Credito"}
            };
            pagos.ForEach(tipos => context.TiposPagos.Add(tipos));
            context.SaveChanges();

            //Iicialización de Clientes
            var cliente = new List<Clientes>
            {
                new Clientes{ID=1, Nombres="Luis Leyton", Direccion="calle 9", Email="leyton@gmail.com", Telefono="877"},
                new Clientes{ID=2, Nombres="Maria Verjan", Direccion="kr 9#32-4", Email="maria@gmail.com", Telefono="32456789"},
                new Clientes{ID=3, Nombres="Auracaris sa", Direccion="Yerbabuena", Email="Aura@gmail.com", Telefono="3030980"},
                new Clientes{ID=4, Nombres="Laura Paola Garzon", Direccion="calle 23# 1328", Email="laura@gmail.com", Telefono="349876"},
                new Clientes{ID=5, Nombres="Andrea Cuellar", Direccion="calle 23# 1328", Email="andrea@hotmail.com", Telefono="457893"},
                new Clientes{ID=6, Nombres="Jose Antonio", Direccion="Kr 23# 13", Email="joseantonio@gmail.com", Telefono="1340987"},
                new Clientes{ID=7, Nombres="Yilmer Ramires", Direccion="calle 90# 13b-988", Email="ramirez@gmail.com", Telefono="2347689"},
                new Clientes{ID=8, Nombres="Juan Pablo Perez", Direccion="kr 23# 238", Email="juanpablo@gmail.com", Telefono="2345698"},
                new Clientes{ID=9, Nombres="Dora Castillo", Direccion="calle 23# 1328", Email="doris@gmail.com", Telefono="8907654"},
                new Clientes{ID=10, Nombres="Melba Guarnizo", Direccion="calle 23# 1328", Email="laura@gmail.com", Telefono="4567823"}
            };
            cliente.ForEach(clien => context.Clientes.Add(clien));
            context.SaveChanges();

            //Inicialización de Facturas
            var facturas = new List<Facturas>
            {
                new Facturas {ID=1, Fecha=Convert.ToDateTime ("2015-07-01"), ValorTotal=13000, ClientesID=1,TiposPagosID=2},
                new Facturas {ID=2, Fecha=Convert.ToDateTime ("2015-07-01"), ValorTotal=80000, ClientesID=2,TiposPagosID=1},
                new Facturas {ID=3, Fecha=Convert.ToDateTime ("2015-07-10"), ValorTotal=3000, ClientesID=8,TiposPagosID=2},
                new Facturas {ID=4, Fecha=Convert.ToDateTime ("2015-07-02"), ValorTotal=10000, ClientesID=4,TiposPagosID=1},
                new Facturas {ID=5, Fecha=Convert.ToDateTime ("2015-07-21"), ValorTotal=17000, ClientesID=1,TiposPagosID=2},
                new Facturas {ID=6, Fecha=Convert.ToDateTime ("2015-07-22"), ValorTotal=2000, ClientesID=2,TiposPagosID=1}
            };
            facturas.ForEach(fac => context.Facturas.Add(fac));
            context.SaveChanges();

            //Inicialización de detalles Facturas
            var dtFactura = new List<DetalleFacturas>
            {
                new DetalleFacturas{ID=1, Cantidad=10, Valor= 5000, ValorTotal=5000, FacturasID=1, ProductosID=2},
                new DetalleFacturas{ID=2, Cantidad=5, Valor= 400, ValorTotal=5000, FacturasID=2, ProductosID=1},
                new DetalleFacturas{ID=3, Cantidad=100, Valor= 500, ValorTotal=5000, FacturasID=3, ProductosID=4},
                new DetalleFacturas{ID=4, Cantidad=1, Valor= 500, ValorTotal=5000, FacturasID=4, ProductosID=5},
                new DetalleFacturas{ID=4, Cantidad=24, Valor= 500, ValorTotal=5000, FacturasID=5, ProductosID=3},
                new DetalleFacturas{ID=6, Cantidad=12, Valor= 500, ValorTotal=5000, FacturasID=6, ProductosID=4}
            };
            dtFactura.ForEach(dtfact => context.DetalleFacturas.Add(dtfact));
            context.SaveChanges();

            //Inicialización  puntos clientes

            var puntosClient = new List<PuntosClientes>
            {
                new PuntosClientes{ID=1, PuntosDisponibles=100, PuntosRedimidos=30, PuntosTotales= 130, ClientesID=1},
                new PuntosClientes{ID=1, PuntosDisponibles=400, PuntosRedimidos=500, PuntosTotales= 900, ClientesID=2},
                new PuntosClientes{ID=1, PuntosDisponibles=500, PuntosRedimidos=100, PuntosTotales= 600, ClientesID=8},
                new PuntosClientes{ID=1, PuntosDisponibles=400, PuntosRedimidos=100, PuntosTotales= 530, ClientesID=4},
                new PuntosClientes{ID=1, PuntosDisponibles=400, PuntosRedimidos=30, PuntosTotales= 430, ClientesID=1},
                new PuntosClientes{ID=1, PuntosDisponibles=10, PuntosRedimidos=30, PuntosTotales= 40, ClientesID=2}
            };
            puntosClient.ForEach(puntosCli => context.PuntosClientes.Add(puntosCli));
            context.SaveChanges();

            //Inicializacion  Regla de Fidelizacion.
            var reglaFid = new List<ReglaFidelizacion>
            {
                new ReglaFidelizacion{ID=1, Cantidad=10, ProductosID=1, Puntos=50}
            };
            reglaFid.ForEach(regla => context.ReglaFidelizacion.Add(regla));
            context.SaveChanges();
        //Inicializacion de Detalles Puntos 
            var dtPuntos = new List<DetallePuntos>
            {
                new DetallePuntos {ID=1, Fecha=Convert.ToDateTime("2015-07-01"), PuntosClientesID=1, ReglaFidelizacionID=1}
            };
            dtPuntos.ForEach(dtPunt => context.DetallePuntos.Add(dtPunt));
            context.SaveChanges();

            //inicialización PedidosClientes 
            var pedidosClient = new List<PedidosClientes>
            {
                new PedidosClientes{ID=1, ClientesID=1, Fecha=Convert.ToDateTime("2015-07-01"), FechaEntrega= Convert.ToDateTime("2015-07-13"), Abono=100000, ValorTotal=200000, EstadosPedidoID=1}
            };
            pedidosClient.ForEach(pedido => context.PedidosClientes.Add(pedido));
            context.SaveChanges();
            //Inicialización de DetallesPedidos

            var dtPedidos = new List<DetallePedidos>
            {
                new DetallePedidos{ID=1, Descripcion="300 Pasteles de pollo", Cantidad=300, Valor=1000, ValorTotal=300000, ProductoSID=1, PedidosClientesID=1}
            };
            dtPedidos.ForEach(dtPedido => context.DetallePedidos.Add(dtPedido));
            context.SaveChanges();
           
            //Inicializacio de Compras
            var compras = new List<Compras>
            {
                new Compras{ID=1, Cantidad=92, Fecha= Convert.ToDateTime("2015-07-02"), FechaEntrega=Convert.ToDateTime("2015-07-06"), Marca="Haz De Oro", Valor=72000, ValorTotal=144000, FechaVencimiento=Convert.ToDateTime("2017-01-08"), MateriasPrimasID=2, ProveedoresID= 7, EstadosComprasID=1 },
                new Compras{ID=2, Cantidad=240, Fecha= Convert.ToDateTime("2015-07-02"), FechaEntrega=Convert.ToDateTime("2015-07-06"), Marca="huevos tipo A", Valor=25, ValorTotal=56000, FechaVencimiento=Convert.ToDateTime("2017-01-08"), MateriasPrimasID=3, ProveedoresID= 4, EstadosComprasID=1 },
                new Compras{ID=3, Cantidad=4, Fecha= Convert.ToDateTime("2015-07-02"), FechaEntrega=Convert.ToDateTime("2015-07-06"), Marca="Queso Costeño", Valor=5000, ValorTotal=20000, FechaVencimiento=Convert.ToDateTime("2017-01-08"), MateriasPrimasID=5, ProveedoresID= 7, EstadosComprasID=1 },
                new Compras{ID=4, Cantidad=46, Fecha= Convert.ToDateTime("2015-07-02"), FechaEntrega=Convert.ToDateTime("2015-07-06"), Marca="HojalPan", Valor=5000, ValorTotal=20000, FechaVencimiento=Convert.ToDateTime("2017-01-08"), MateriasPrimasID=4, ProveedoresID= 7, EstadosComprasID=1 },
                new Compras{ID=5, Cantidad=60, Fecha= Convert.ToDateTime("2015-07-02"), FechaEntrega=Convert.ToDateTime("2015-07-06"), Marca="Cocacola 350", Valor=1000, ValorTotal=60000, FechaVencimiento=Convert.ToDateTime("2017-01-08"), MateriasPrimasID=1, ProveedoresID= 1, EstadosComprasID=1 }
            };
            compras.ForEach(compr => context.Compras.Add(compr));
            context.SaveChanges();

            
        }   
    }
}