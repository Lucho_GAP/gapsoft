﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace GAPSoft.Models
{
    public class EstadosIva
    {
        public int ID { get; set; }

        [System.ComponentModel.DisplayName("Descripción")]
        [Required, StringLength(50, MinimumLength = 1, ErrorMessage = "El campo debe tener mínimo UN carácter y máximo 50")]
        public string Descripcion { get; set; }
    }
}