﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace GAPSoft.Models
{
    public class TiposIva
    {
        public int ID { get; set; }

        [DisplayName("Descripción")]
        [Required, StringLength(50, MinimumLength = 1, ErrorMessage = "El campo debe tener mínimo UN carácter y máximo 50")]
        public string  Descripcion { get; set; }

        [Range(0, 99999)]
        [DataType(DataType.Currency, ErrorMessage = "El Campo solo permite Números")]
        [RegularExpression("[0-9]*", ErrorMessage = "El campo solo permite Números Enteros")]
        public int Iva { get; set; }

        public int EstadosIvaID { get; set; }

        public virtual EstadosIva  EstadoIva{ get; set; }

    }
}