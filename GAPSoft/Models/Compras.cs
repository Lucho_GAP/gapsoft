﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel;
using System.Linq;
using System.Web;

namespace GAPSoft.Models
{
    public class Compras
    {
        public int ID { get; set; }

        [DataType(DataType.Date)]
        [DisplayFormat(DataFormatString = "{0:yyyy-MM-dd}", ApplyFormatInEditMode = true)]
        public DateTime Fecha { get; set; }
        
        [DisplayName("Fecha de Entrega")]
        [DataType(DataType.Date)]
        [DisplayFormat(DataFormatString = "{0:yyyy-MM-dd}", ApplyFormatInEditMode = true)]
        public DateTime FechaEntrega { get; set; }

        [Range(1, 99999)]
        [DataType(DataType.Currency, ErrorMessage = "El Campo solo permite Números")]
        [RegularExpression("[0-9]*", ErrorMessage = "El campo solo permite Números Enteros")]
        public int Cantidad { get; set; }

        [Range(1, 999999999)]
        [DataType(DataType.Currency, ErrorMessage = "El Campo solo permite Números")]
        public float Valor { get; set; }

        [DisplayName("Valor Total")]
        [Range(1, 999999999)]
        [DataType(DataType.Currency, ErrorMessage = "El Campo solo permite Números")]
        public float ValorTotal { get; set; }

        [DisplayName("Marca del Producto")]
        [DataType(DataType.Text)]
        public string Marca { get; set; }

        [DisplayName("Fecha de Vencimiento")]
        [DataType(DataType.Date)]
        [DisplayFormat(DataFormatString = "{0:yyyy-MM-dd}", ApplyFormatInEditMode = true)]
        public DateTime FechaVencimiento { get; set; }


        public int EstadosComprasID { get; set; }
        public int ProveedoresID { get; set; }
        public int MateriasPrimasID { get; set; }
        public virtual EstadosCompras EstadoCompra { get; set; }
        public virtual  MateriasPrimas materiaPrima { get; set; }
        public  virtual Proveedores Proveedor { get; set; }
    }
}