﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel;
using System.Linq;
using System.Web;

namespace GAPSoft.Models
{
    public class Proveedores
    {
        public int ID { get; set; }
        [Required]
        [DisplayName("Descripción")]
        [StringLength(200, MinimumLength = 1, ErrorMessage = "El campo debe tener mínimo UN carácter y máximo 200")]
        
        //[DataType(DataType.Text)]
        [RegularExpression(@"^[a-zA-Z''-''.'\s]*$", ErrorMessage = " La Descripción solo Permite Letras")]
        public string  Descripcion { get; set; }

        [DisplayName("Télefono")]
        //[DataType(DataType.PhoneNumber)]
        [RegularExpression(@"^[0-9]{0,15}$", ErrorMessage = "Definir solo numeros")]
        [StringLength(32)]
        public string  Telefono { get; set; }

        [Required]
       // [DataType(DataType.EmailAddress, ErrorMessage = "Mail incorrecto")]
        [EmailAddress(ErrorMessage = "Mail incorrecto!")]
        [StringLength(200, MinimumLength = 1, ErrorMessage = "El campo debe tener mínimo UN carácter y máximo 200")]
        [DisplayName("E-Mail")]
        public string Email { get; set; }

        [Required]
        [StringLength(200, MinimumLength = 1, ErrorMessage = "El campo debe tener mínimo UN carácter y máximo 200")]
        [DataType(DataType.Text)]
        [RegularExpression(@"^[A-Z]+[a-zA-Z''-'\s]*$", ErrorMessage = " El nombre solo Permite Letras, y la primera es en 'MAYUSCULA' ")]
        public string Contacto { get; set; }

        [DisplayName("Estados")]
        public int EstadosClientesID { get; set; }

         [DisplayName("Estado")]
        public virtual EstadosClientes EstadoCliente { get; set; }
        


    }
}