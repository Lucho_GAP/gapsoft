﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace GAPSoft.Models
{
    public class DetalleComponentes
    {
        public int ID { get; set; }

        [Range(1, 99999)]
        [DataType(DataType.Currency, ErrorMessage = "El Campo solo permite Números")]
        [RegularExpression("[0-9]*", ErrorMessage = "El campo solo permite Números Enteros")]
        public int Cantidad { get; set; }

        public int ComponentesID { get; set; }
        public int MateriasPrimasID { get; set; }

        public virtual  Componentes Componente { get; set; }
        public virtual MateriasPrimas MateriaPrima{ get; set; }
    }
}