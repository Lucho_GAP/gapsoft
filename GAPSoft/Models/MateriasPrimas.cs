﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace GAPSoft.Models
{
    public class MateriasPrimas
    {
        public int ID { get; set; }

        [DisplayName("Descripción")]
        [StringLength(200, MinimumLength = 1, ErrorMessage = "El campo debe tener mínimo UN carácter y máximo 200")]
        //[DataType(DataType.Text)]
        //[RegularExpression(@"^[a-zA-Z''-''.'\s]*$", ErrorMessage = " La Descripción solo Permite Letras")]
        public string Descripcion { get; set; }

        [Range(1, 99999)]
        [DataType(DataType.Currency, ErrorMessage = "El Campo solo permite Números")]
        [RegularExpression("[0-9]*", ErrorMessage = "El campo solo permite Números Enteros")]
        public int Cantidad { get; set; }

        [Range(1, 99999)]
        [DataType(DataType.Currency, ErrorMessage = "El Campo solo permite Números")]
        [RegularExpression("[0-9]*", ErrorMessage = "El campo solo permite Números Enteros")]
        public int Stock { get; set; }

        [DisplayName("Stock de Compra")]
        [Range(1, 99999)]
        [DataType(DataType.Currency, ErrorMessage = "El Campo solo permite Números")]
        [RegularExpression("[0-9]*", ErrorMessage = "El campo solo permite Números Enteros")]
        public int StockCompra { get; set; }

        [DisplayName("Precio de Compra")]
        [Range(1, 999999999)]
        [DataType(DataType.Currency, ErrorMessage = "El Campo solo permite Números")]
        public float PrecioCompra { get; set; }


        public int EstadosComprasID { get; set; }

        public virtual  EstadosCompras EstadoCompra { get; set; }
    }
}