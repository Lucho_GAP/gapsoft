﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace GAPSoft.Models
{
    public class ReglaFidelizacion
    {
        public int ID { get; set; }
        [Range(1, 99999)]
        [DataType(DataType.Currency, ErrorMessage = "El Campo solo permite Números")]
        [RegularExpression("[0-9]*", ErrorMessage = "El campo solo permite Números Enteros")]
        public int Cantidad { get; set; }

        [Range(1, 999999999)]
        [DataType(DataType.Currency, ErrorMessage = "El Campo solo permite Números")]
        public int Puntos { get; set; }

        public int ProductosID { get; set; }

        public virtual Productos Producto { get; set; }
    }
}