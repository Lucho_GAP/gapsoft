﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;

using System.Linq;
using System.Web;

namespace GAPSoft.Models
{
    public class PedidosClientes
    {
        public int ID { get; set; }
        [DataType(DataType.Date)]
        [DisplayFormat(DataFormatString = "{0:yyyy-MM-dd}", ApplyFormatInEditMode = true)]
        public DateTime Fecha { get; set; }

        [DisplayName("Fecha de Entrega")]
        [DataType(DataType.Date)]
        [DisplayFormat(DataFormatString = "{0:yyyy-MM-dd}", ApplyFormatInEditMode = true)]
        public DateTime FechaEntrega { get; set; }

        [Range(1,999999999)]
        [DataType(DataType.Currency, ErrorMessage= "El Campo solo permite Números")]
        public float Abono { get; set; }

        [DisplayName("Valor Total")]
        [Range(1, 999999999)]
        [DataType(DataType.Currency, ErrorMessage = "El Campo solo permite Números")]
        public float ValorTotal { get; set; }

        [DisplayName("Cliente")]
        public int ClientesID  { get; set; }


        public int EstadosPedidoID { get; set; }
        

        public virtual Clientes Cliente { get; set; }
        public virtual EstadosPedidos EstadoPedido{ get; set; }

        
    }
}