﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Web;

namespace GAPSoft.Models
{
    public class DetallePedidos
    {
        public int ID { get; set; }
        [Required]
        [StringLength(200, MinimumLength = 1, ErrorMessage = "El campo debe tener minimo UN caracter y maximo 200")]
        public string Descripcion { get; set; }

        [Range(1, 99999)]
        [DataType(DataType.Currency, ErrorMessage = "El Campo solo permite Números")]
        [RegularExpression("[0-9]*", ErrorMessage = "El campo solo permite Números Enteros")]
        public int Cantidad { get; set; }

        [Range(1, 999999999)]
        [DataType(DataType.Currency, ErrorMessage = "El Campo solo permite Números")]
        public float Valor { get; set; }

        [DisplayName("Valor Total")]
        [Range(1, 999999999)]
        [DataType(DataType.Currency, ErrorMessage = "El Campo solo permite Números")]
        public float ValorTotal { get; set; }


        public int ProductoSID { get; set; }
        public int PedidosClientesID { get; set; }

        public virtual Productos Producto{ get; set; }
        public virtual PedidosClientes PedidoCliente { get; set; }
    }
}