﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace GAPSoft.Models
{
    public class DetallePuntos
    {
        public int ID { get; set; }

        [DataType(DataType.Date)]
        [DisplayFormat(DataFormatString = "{0:yyyy-MM-dd}", ApplyFormatInEditMode = true)]
        public DateTime Fecha { get; set; }

        public int PuntosClientesID { get; set; }
        public int ReglaFidelizacionID { get; set; }

        public virtual PuntosClientes PuntosCliente { get; set; }
        public virtual ReglaFidelizacion ReFidelizacion { get; set; }
    }
}