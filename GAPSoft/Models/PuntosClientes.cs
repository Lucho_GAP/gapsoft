﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace GAPSoft.Models
{
    public class PuntosClientes
    {
        public int ID { get; set; }

        [DisplayName("Puntos Clientes")]
        [Range(1, 999999999)]
       // [DataType(DataType.Currency, ErrorMessage = "El Campo solo permite Números")]
        [RegularExpression("[0-9]*",ErrorMessage= "El campo solo permite Números Enteros")]
        public int PuntosDisponibles { get; set; }

        [DisplayName("Total de Puntos")]
        [Range(1, 999999999)]
        //[DataType(DataType.Currency, ErrorMessage = "El Campo solo permite Números")]
        [RegularExpression("[0-9]*", ErrorMessage = "El campo solo permite Números Enteros")]
        public int PuntosTotales { get; set; }

        [DisplayName("Puntos Gastados")]
        [Range(1, 99999999,ErrorMessage="aaa")]
       // [DataType(DataType.Currency, ErrorMessage = "El Campo solo permite Números")]
        [RegularExpression("[0-9]*", ErrorMessage = "El campo solo permite Números Enteros")]
        public int  PuntosRedimidos { get; set; }

        [Editable(false)]
        public int ClientesID { get; set; }

        public virtual Clientes Cliente { get; set; }
    }
}