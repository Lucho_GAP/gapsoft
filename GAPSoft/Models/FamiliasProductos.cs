﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace GAPSoft.Models
{
    public class FamiliasProductos
    {
        public int ID { get; set; }

        [DisplayName("Descripción")]
        [Required, StringLength(200, MinimumLength = 1, ErrorMessage = "El campo debe tener mínimo UN carácter y máximo 200")]
        public string Descripcion { get; set; }



        public  int EstadosProductoID  { get; set; }

        public virtual EstadosProductos EstadoProducto { get; set; }
    }
}