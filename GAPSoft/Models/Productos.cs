﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace GAPSoft.Models
{
    public class Productos
    {
        public int ID { get; set; }

        [DisplayName("Descripción")]
        [Required, StringLength(200, MinimumLength = 1, ErrorMessage = "El campo debe tener mínimo UN carácter y máximo 200")]
        public string Descripcion { get; set; }

        [DisplayName("Precio Venta")]
        [Range(1, 999999999)]
        [DataType(DataType.Currency, ErrorMessage = "El Campo solo permite Números")]
        public float PrecioVenta { get; set; }

        [Range(1, 99999)]
        [DataType(DataType.Currency, ErrorMessage = "El Campo solo permite Números")]
        [RegularExpression("[0-9]*", ErrorMessage = "El campo solo permite Números Enteros")]
        public int Stock { get; set; }

        [DisplayName("Stock de Pedido")]
        [Range(1, 99999)]
        [DataType(DataType.Currency, ErrorMessage = "El Campo solo permite Números")]
        [RegularExpression("[0-9]*", ErrorMessage = "El campo solo permite Números Enteros")]
        public int StockPedido { get; set; }

        public int GruposProductosID { get; set; }
        public int EstadosPedidosID { get; set; }
        public int TiposIvaID { get; set; }

       
        public virtual GruposProductos Grupo { get; set; }
        public virtual EstadosPedidos EstadoPedido { get; set; }
        public virtual TiposIva TipoIva { get; set; }

    }
}