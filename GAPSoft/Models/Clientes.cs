﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;

namespace GAPSoft.Models
{
    public class Clientes
    {
        public int ID { get; set; }

        [Required]
        [StringLength(200, MinimumLength = 1, ErrorMessage = "El campo debe tener mínimo UN carácter y máximo 200")]
        [DataType(DataType.Text)]
        [RegularExpression(@"^[A-Z]+[a-zA-Z''-'\s]*$",ErrorMessage=" El nombre solo Permite Letras, y la primera es en 'MAYUSCULA' ")]
        public string Nombres { get; set; }

        [DisplayName("Télefono")]
        //[Phone]
        //[DataType(DataType.PhoneNumber)]
        [RegularExpression(@"^[0-9]{0,15}$", ErrorMessage = "Definir solo numeros")]
        [StringLength(32)]
        public string Telefono { get; set; }

       [Required]
       [StringLength(200, MinimumLength = 1, ErrorMessage = "El campo debe tener mínimo UN carácter y máximo 200")]
       //[RegularExpression("\b[A-Z0-9._%+-]+@[A-Z0-9.-]+\\.[A-Z]{2,4}\b", ErrorMessage="Mail incorrecto")]
       [DisplayName ("E-Mail")]
       public string Email { get; set; }

       [Required]
       [StringLength(200, MinimumLength = 1, ErrorMessage = "El campo debe tener mínimo UN carácter y máximo 200")]
       [DisplayName("Dirección")]
       public string Direccion { get; set; }
    }
}