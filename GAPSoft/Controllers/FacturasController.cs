﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using GAPSoft.DAL;
using GAPSoft.Models;

namespace GAPSoft.Controllers
{
    public class FacturasController : Controller
    {
        private GATSoftContext db = new GATSoftContext();

        //// GET: Facturas
        //public ActionResult Index()
        //{
        //    var facturas = db.Facturas.Include(f => f.Cliente).Include(f => f.TipoPago);
        //    return View(facturas.ToList());
        //}
        // GET: Facturas/5
        public ActionResult Index(int? idCliente)
        {
            if (idCliente != null)
            {
                var facturas = db.Facturas.Where(f => f.ClientesID == idCliente).Include(f => f.Cliente).Include(f => f.TipoPago);
                return View(facturas.ToList());
            }
            else
            {
                var facturas = db.Facturas.Include(f => f.Cliente).Include(f => f.TipoPago);
                return View(facturas.ToList());
            }
        }

        // GET: Facturas/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Facturas facturas = db.Facturas.Find(id);
            if (facturas == null)
            {
                return HttpNotFound();
            }
            return View(facturas);
        }

        // GET: Facturas/Create
        public ActionResult Create()
        {
            ViewBag.ClientesID = new SelectList(db.Clientes, "ID", "Nombres");
            ViewBag.TiposPagosID = new SelectList(db.TiposPagos, "ID", "Descripcion");
            return View();
        }

        // POST: Facturas/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "ID,Fecha,ValorTotal,ClientesID,TiposPagosID")] Facturas facturas)
        {
            if (ModelState.IsValid)
            {
                db.Facturas.Add(facturas);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            ViewBag.ClientesID = new SelectList(db.Clientes, "ID", "Nombres", facturas.ClientesID);
            ViewBag.TiposPagosID = new SelectList(db.TiposPagos, "ID", "Descripcion", facturas.TiposPagosID);
            return View(facturas);
        }

        // GET: Facturas/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Facturas facturas = db.Facturas.Find(id);
            if (facturas == null)
            {
                return HttpNotFound();
            }
            ViewBag.ClientesID = new SelectList(db.Clientes, "ID", "Nombres", facturas.ClientesID);
            ViewBag.TiposPagosID = new SelectList(db.TiposPagos, "ID", "Descripcion", facturas.TiposPagosID);
            return View(facturas);
        }

        // POST: Facturas/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "ID,Fecha,ValorTotal,ClientesID,TiposPagosID")] Facturas facturas)
        {
            if (ModelState.IsValid)
            {
                db.Entry(facturas).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.ClientesID = new SelectList(db.Clientes, "ID", "Nombres", facturas.ClientesID);
            ViewBag.TiposPagosID = new SelectList(db.TiposPagos, "ID", "Descripcion", facturas.TiposPagosID);
            return View(facturas);
        }

        // GET: Facturas/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Facturas facturas = db.Facturas.Find(id);
            if (facturas == null)
            {
                return HttpNotFound();
            }
            return View(facturas);
        }

        // POST: Facturas/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Facturas facturas = db.Facturas.Find(id);
            db.Facturas.Remove(facturas);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
