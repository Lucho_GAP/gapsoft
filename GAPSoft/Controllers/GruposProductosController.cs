﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using GAPSoft.DAL;
using GAPSoft.Models;

namespace GAPSoft.Controllers
{
    public class GruposProductosController : Controller
    {
        private GATSoftContext db = new GATSoftContext();

        // GET: GruposProductos
        public ActionResult Index()
        {
            var gruposProductos = db.GruposProductos.Include(g => g.EstadoProducto).Include(g => g.Familia);
            return View(gruposProductos.ToList());
        }

        // GET: GruposProductos/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            GruposProductos gruposProductos = db.GruposProductos.Find(id);
            if (gruposProductos == null)
            {
                return HttpNotFound();
            }
            return View(gruposProductos);
        }

        // GET: GruposProductos/Create
        public ActionResult Create()
        {
            ViewBag.EstadosProductosID = new SelectList(db.EstadosProductos, "ID", "Descripcion");
            ViewBag.FamiliasProductosID = new SelectList(db.FamiliasProductos, "ID", "Descripcion");
            return View();
        }

        // POST: GruposProductos/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "ID,Descripcion,FamiliasProductosID,EstadosProductosID")] GruposProductos gruposProductos)
        {
            if (ModelState.IsValid)
            {
                db.GruposProductos.Add(gruposProductos);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            ViewBag.EstadosProductosID = new SelectList(db.EstadosProductos, "ID", "Descripcion", gruposProductos.EstadosProductosID);
            ViewBag.FamiliasProductosID = new SelectList(db.FamiliasProductos, "ID", "Descripcion", gruposProductos.FamiliasProductosID);
            return View(gruposProductos);
        }

        // GET: GruposProductos/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            GruposProductos gruposProductos = db.GruposProductos.Find(id);
            if (gruposProductos == null)
            {
                return HttpNotFound();
            }
            ViewBag.EstadosProductosID = new SelectList(db.EstadosProductos, "ID", "Descripcion", gruposProductos.EstadosProductosID);
            ViewBag.FamiliasProductosID = new SelectList(db.FamiliasProductos, "ID", "Descripcion", gruposProductos.FamiliasProductosID);
            return View(gruposProductos);
        }

        // POST: GruposProductos/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "ID,Descripcion,FamiliasProductosID,EstadosProductosID")] GruposProductos gruposProductos)
        {
            if (ModelState.IsValid)
            {
                db.Entry(gruposProductos).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.EstadosProductosID = new SelectList(db.EstadosProductos, "ID", "Descripcion", gruposProductos.EstadosProductosID);
            ViewBag.FamiliasProductosID = new SelectList(db.FamiliasProductos, "ID", "Descripcion", gruposProductos.FamiliasProductosID);
            return View(gruposProductos);
        }

        // GET: GruposProductos/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            GruposProductos gruposProductos = db.GruposProductos.Find(id);
            if (gruposProductos == null)
            {
                return HttpNotFound();
            }
            return View(gruposProductos);
        }

        // POST: GruposProductos/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            GruposProductos gruposProductos = db.GruposProductos.Find(id);
            db.GruposProductos.Remove(gruposProductos);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
