﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using GAPSoft.DAL;
using GAPSoft.Models;

namespace GAPSoft.Controllers
{
    public class ComprasController : Controller
    {
        private GATSoftContext db = new GATSoftContext();

        // GET: Compras
        public ActionResult Index()
        {
            var compras = db.Compras.Include(c => c.EstadoCompra).Include(c => c.materiaPrima).Include(c => c.Proveedor);
            return View(compras.ToList());
        }

        // GET: Compras/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Compras compras = db.Compras.Find(id);
            if (compras == null)
            {
                return HttpNotFound();
            }
            return View(compras);
        }

        // GET: Compras/Create
        public ActionResult Create()
        {
            ViewBag.EstadosComprasID = new SelectList(db.EstadosCompras, "ID", "Descripcion");
            ViewBag.MateriasPrimasID = new SelectList(db.MateriasPrimas, "ID", "Descripcion");
            ViewBag.ProveedoresID = new SelectList(db.Proveedores, "ID", "Descripcion");
            return View();
        }

        // POST: Compras/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "ID,Fecha,FechaEntrega,Cantidad,Valor,ValorTotal,Marca,FechaVencimiento,ProveedoresID,MateriasPrimasID,EstadosComprasID")] Compras compras)
        {
            if (ModelState.IsValid)
            {
                db.Compras.Add(compras);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            ViewBag.EstadosComprasID = new SelectList(db.EstadosCompras, "ID", "Descripcion", compras.EstadosComprasID);
            ViewBag.MateriasPrimasID = new SelectList(db.MateriasPrimas, "ID", "Descripcion", compras.MateriasPrimasID);
            ViewBag.ProveedoresID = new SelectList(db.Proveedores, "ID", "Descripcion", compras.ProveedoresID);
            return View(compras);
        }

        // GET: Compras/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Compras compras = db.Compras.Find(id);
            if (compras == null)
            {
                return HttpNotFound();
            }
            ViewBag.EstadosComprasID = new SelectList(db.EstadosCompras, "ID", "Descripcion", compras.EstadosComprasID);
            ViewBag.MateriasPrimasID = new SelectList(db.MateriasPrimas, "ID", "Descripcion", compras.MateriasPrimasID);
            ViewBag.ProveedoresID = new SelectList(db.Proveedores, "ID", "Descripcion", compras.ProveedoresID);
            return View(compras);
        }

        // POST: Compras/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "ID,Fecha,FechaEntrega,Cantidad,Valor,ValorTotal,Marca,FechaVencimiento,ProveedoresID,MateriasPrimasID,EstadosComprasID")] Compras compras)
        {
            if (ModelState.IsValid)
            {
                db.Entry(compras).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.EstadosComprasID = new SelectList(db.EstadosCompras, "ID", "Descripcion", compras.EstadosComprasID);
            ViewBag.MateriasPrimasID = new SelectList(db.MateriasPrimas, "ID", "Descripcion", compras.MateriasPrimasID);
            ViewBag.ProveedoresID = new SelectList(db.Proveedores, "ID", "Descripcion", compras.ProveedoresID);
            return View(compras);
        }

        // GET: Compras/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Compras compras = db.Compras.Find(id);
            if (compras == null)
            {
                return HttpNotFound();
            }
            return View(compras);
        }

        // POST: Compras/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Compras compras = db.Compras.Find(id);
            db.Compras.Remove(compras);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
