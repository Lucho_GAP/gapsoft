﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using GAPSoft.DAL;
using GAPSoft.Models;

namespace GAPSoft.Controllers
{
    public class ProduccionsController : Controller
    {
        private GATSoftContext db = new GATSoftContext();

        // GET: Produccions
        public ActionResult Index()
        {
            var produccion = db.Produccion.Include(p => p.Producto);
            return View(produccion.ToList());
        }

        // GET: Produccions/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Produccion produccion = db.Produccion.Find(id);
            if (produccion == null)
            {
                return HttpNotFound();
            }
            return View(produccion);
        }

        // GET: Produccions/Create
        public ActionResult Create()
        {
            ViewBag.ProductosID = new SelectList(db.Productos, "ID", "Descripcion");
            return View();
        }

        // POST: Produccions/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "ID,Cantidad,Fecha,ProductosID")] Produccion produccion)
        {
            if (ModelState.IsValid)
            {
                db.Produccion.Add(produccion);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            ViewBag.ProductosID = new SelectList(db.Productos, "ID", "Descripcion", produccion.ProductosID);
            return View(produccion);
        }

        // GET: Produccions/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Produccion produccion = db.Produccion.Find(id);
            if (produccion == null)
            {
                return HttpNotFound();
            }
            ViewBag.ProductosID = new SelectList(db.Productos, "ID", "Descripcion", produccion.ProductosID);
            return View(produccion);
        }

        // POST: Produccions/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "ID,Cantidad,Fecha,ProductosID")] Produccion produccion)
        {
            if (ModelState.IsValid)
            {
                db.Entry(produccion).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.ProductosID = new SelectList(db.Productos, "ID", "Descripcion", produccion.ProductosID);
            return View(produccion);
        }

        // GET: Produccions/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Produccion produccion = db.Produccion.Find(id);
            if (produccion == null)
            {
                return HttpNotFound();
            }
            return View(produccion);
        }

        // POST: Produccions/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Produccion produccion = db.Produccion.Find(id);
            db.Produccion.Remove(produccion);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
