﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using GAPSoft.DAL;
using GAPSoft.Models;

namespace GAPSoft.Controllers
{
    public class MateriasPrimasController : Controller
    {
        private GATSoftContext db = new GATSoftContext();

        // GET: MateriasPrimas
        public ActionResult Index()
        {
            var materiasPrimas = db.MateriasPrimas.Include(m => m.EstadoCompra);
            return View(materiasPrimas.ToList());
        }

        // GET: MateriasPrimas/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            MateriasPrimas materiasPrimas = db.MateriasPrimas.Find(id);
            if (materiasPrimas == null)
            {
                return HttpNotFound();
            }
            return View(materiasPrimas);
        }

        // GET: MateriasPrimas/Create
        public ActionResult Create()
        {
            ViewBag.EstadosComprasID = new SelectList(db.EstadosCompras, "ID", "Descripcion");
            return View();
        }

        // POST: MateriasPrimas/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "ID,Descripcion,Cantidad,Stock,StockCompra,PrecioCompra,EstadosComprasID")] MateriasPrimas materiasPrimas)
        {
            if (ModelState.IsValid)
            {
                db.MateriasPrimas.Add(materiasPrimas);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            ViewBag.EstadosComprasID = new SelectList(db.EstadosCompras, "ID", "Descripcion", materiasPrimas.EstadosComprasID);
            return View(materiasPrimas);
        }

        // GET: MateriasPrimas/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            MateriasPrimas materiasPrimas = db.MateriasPrimas.Find(id);
            if (materiasPrimas == null)
            {
                return HttpNotFound();
            }
            ViewBag.EstadosComprasID = new SelectList(db.EstadosCompras, "ID", "Descripcion", materiasPrimas.EstadosComprasID);
            return View(materiasPrimas);
        }

        // POST: MateriasPrimas/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "ID,Descripcion,Cantidad,Stock,StockCompra,PrecioCompra,EstadosComprasID")] MateriasPrimas materiasPrimas)
        {
            if (ModelState.IsValid)
            {
                db.Entry(materiasPrimas).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.EstadosComprasID = new SelectList(db.EstadosCompras, "ID", "Descripcion", materiasPrimas.EstadosComprasID);
            return View(materiasPrimas);
        }

        // GET: MateriasPrimas/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            MateriasPrimas materiasPrimas = db.MateriasPrimas.Find(id);
            if (materiasPrimas == null)
            {
                return HttpNotFound();
            }
            return View(materiasPrimas);
        }

        // POST: MateriasPrimas/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            MateriasPrimas materiasPrimas = db.MateriasPrimas.Find(id);
            db.MateriasPrimas.Remove(materiasPrimas);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
