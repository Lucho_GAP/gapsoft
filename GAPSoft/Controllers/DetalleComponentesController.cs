﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using GAPSoft.DAL;
using GAPSoft.Models;

namespace GAPSoft.Controllers
{
    public class DetalleComponentesController : Controller
    {
        private GATSoftContext db = new GATSoftContext();

        // GET: DetalleComponentes
        public ActionResult Index()
        {
            var detalleComponentes = db.DetalleComponentes.Include(d => d.Componente).Include(d => d.MateriaPrima);
            return View(detalleComponentes.ToList());
        }

        // GET: DetalleComponentes/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            DetalleComponentes detalleComponentes = db.DetalleComponentes.Find(id);
            if (detalleComponentes == null)
            {
                return HttpNotFound();
            }
            return View(detalleComponentes);
        }

        // GET: DetalleComponentes/Create
        public ActionResult Create()
        {
            ViewBag.ComponentesID = new SelectList(db.Componentes, "ID", "ID");
            ViewBag.MateriasPrimasID = new SelectList(db.MateriasPrimas, "ID", "Descripcion");
            return View();
        }

        // POST: DetalleComponentes/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "ID,Cantidad,ComponentesID,MateriasPrimasID")] DetalleComponentes detalleComponentes)
        {
            if (ModelState.IsValid)
            {
                db.DetalleComponentes.Add(detalleComponentes);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            ViewBag.ComponentesID = new SelectList(db.Componentes, "ID", "ID", detalleComponentes.ComponentesID);
            ViewBag.MateriasPrimasID = new SelectList(db.MateriasPrimas, "ID", "Descripcion", detalleComponentes.MateriasPrimasID);
            return View(detalleComponentes);
        }

        // GET: DetalleComponentes/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            DetalleComponentes detalleComponentes = db.DetalleComponentes.Find(id);
            if (detalleComponentes == null)
            {
                return HttpNotFound();
            }
            ViewBag.ComponentesID = new SelectList(db.Componentes, "ID", "ID", detalleComponentes.ComponentesID);
            ViewBag.MateriasPrimasID = new SelectList(db.MateriasPrimas, "ID", "Descripcion", detalleComponentes.MateriasPrimasID);
            return View(detalleComponentes);
        }

        // POST: DetalleComponentes/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "ID,Cantidad,ComponentesID,MateriasPrimasID")] DetalleComponentes detalleComponentes)
        {
            if (ModelState.IsValid)
            {
                db.Entry(detalleComponentes).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.ComponentesID = new SelectList(db.Componentes, "ID", "ID", detalleComponentes.ComponentesID);
            ViewBag.MateriasPrimasID = new SelectList(db.MateriasPrimas, "ID", "Descripcion", detalleComponentes.MateriasPrimasID);
            return View(detalleComponentes);
        }

        // GET: DetalleComponentes/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            DetalleComponentes detalleComponentes = db.DetalleComponentes.Find(id);
            if (detalleComponentes == null)
            {
                return HttpNotFound();
            }
            return View(detalleComponentes);
        }

        // POST: DetalleComponentes/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            DetalleComponentes detalleComponentes = db.DetalleComponentes.Find(id);
            db.DetalleComponentes.Remove(detalleComponentes);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
