﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using GAPSoft.DAL;
using GAPSoft.Models;

namespace GAPSoft.Controllers
{
    public class EstadosIvasController : Controller
    {
        private GATSoftContext db = new GATSoftContext();

        // GET: EstadosIvas
        public ActionResult Index()
        {
            return View(db.EstadosIva.ToList());
        }

        // GET: EstadosIvas/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            EstadosIva estadosIva = db.EstadosIva.Find(id);
            if (estadosIva == null)
            {
                return HttpNotFound();
            }
            return View(estadosIva);
        }

        // GET: EstadosIvas/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: EstadosIvas/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "ID,Descripcion")] EstadosIva estadosIva)
        {
            if (ModelState.IsValid)
            {
                db.EstadosIva.Add(estadosIva);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(estadosIva);
        }

        // GET: EstadosIvas/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            EstadosIva estadosIva = db.EstadosIva.Find(id);
            if (estadosIva == null)
            {
                return HttpNotFound();
            }
            return View(estadosIva);
        }

        // POST: EstadosIvas/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "ID,Descripcion")] EstadosIva estadosIva)
        {
            if (ModelState.IsValid)
            {
                db.Entry(estadosIva).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(estadosIva);
        }

        // GET: EstadosIvas/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            EstadosIva estadosIva = db.EstadosIva.Find(id);
            if (estadosIva == null)
            {
                return HttpNotFound();
            }
            return View(estadosIva);
        }

        // POST: EstadosIvas/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            EstadosIva estadosIva = db.EstadosIva.Find(id);
            db.EstadosIva.Remove(estadosIva);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
