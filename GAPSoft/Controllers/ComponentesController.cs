﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using GAPSoft.DAL;
using GAPSoft.Models;

namespace GAPSoft.Controllers
{
    public class ComponentesController : Controller
    {
        private GATSoftContext db = new GATSoftContext();

        // GET: Componentes
        public ActionResult Index()
        {
            var componentes = db.Componentes.Include(c => c.producto);
            return View(componentes.ToList());
        }

        // GET: Componentes/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Componentes componentes = db.Componentes.Find(id);
            if (componentes == null)
            {
                return HttpNotFound();
            }
            return View(componentes);
        }

        // GET: Componentes/Create
        public ActionResult Create()
        {
            ViewBag.ProductosID = new SelectList(db.Productos, "ID", "Descripcion");
            return View();
        }

        // POST: Componentes/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "ID,ProductosID")] Componentes componentes)
        {
            if (ModelState.IsValid)
            {
                db.Componentes.Add(componentes);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            ViewBag.ProductosID = new SelectList(db.Productos, "ID", "Descripcion", componentes.ProductosID);
            return View(componentes);
        }

        // GET: Componentes/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Componentes componentes = db.Componentes.Find(id);
            if (componentes == null)
            {
                return HttpNotFound();
            }
            ViewBag.ProductosID = new SelectList(db.Productos, "ID", "Descripcion", componentes.ProductosID);
            return View(componentes);
        }

        // POST: Componentes/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "ID,ProductosID")] Componentes componentes)
        {
            if (ModelState.IsValid)
            {
                db.Entry(componentes).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.ProductosID = new SelectList(db.Productos, "ID", "Descripcion", componentes.ProductosID);
            return View(componentes);
        }

        // GET: Componentes/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Componentes componentes = db.Componentes.Find(id);
            if (componentes == null)
            {
                return HttpNotFound();
            }
            return View(componentes);
        }

        // POST: Componentes/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Componentes componentes = db.Componentes.Find(id);
            db.Componentes.Remove(componentes);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
