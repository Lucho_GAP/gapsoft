﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using GAPSoft.DAL;
using GAPSoft.Models;

namespace GAPSoft.Controllers
{
    public class FamiliasProductosController : Controller
    {
        private GATSoftContext db = new GATSoftContext();

        // GET: FamiliasProductos
        public ActionResult Index()
        {
            return View(db.FamiliasProductos.ToList());
        }

        // GET: FamiliasProductos/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            FamiliasProductos familiasProductos = db.FamiliasProductos.Find(id);
            if (familiasProductos == null)
            {
                return HttpNotFound();
            }
            return View(familiasProductos);
        }

        // GET: FamiliasProductos/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: FamiliasProductos/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "ID,Descripcion,EstadosProductoID")] FamiliasProductos familiasProductos)
        {
            if (ModelState.IsValid)
            {
                db.FamiliasProductos.Add(familiasProductos);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(familiasProductos);
        }

        // GET: FamiliasProductos/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            FamiliasProductos familiasProductos = db.FamiliasProductos.Find(id);
            if (familiasProductos == null)
            {
                return HttpNotFound();
            }
            return View(familiasProductos);
        }

        // POST: FamiliasProductos/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "ID,Descripcion,EstadosProductoID")] FamiliasProductos familiasProductos)
        {
            if (ModelState.IsValid)
            {
                db.Entry(familiasProductos).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(familiasProductos);
        }

        // GET: FamiliasProductos/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            FamiliasProductos familiasProductos = db.FamiliasProductos.Find(id);
            if (familiasProductos == null)
            {
                return HttpNotFound();
            }
            return View(familiasProductos);
        }

        // POST: FamiliasProductos/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            FamiliasProductos familiasProductos = db.FamiliasProductos.Find(id);
            db.FamiliasProductos.Remove(familiasProductos);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
