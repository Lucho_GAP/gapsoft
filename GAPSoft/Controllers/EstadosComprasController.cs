﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using GAPSoft.DAL;
using GAPSoft.Models;

namespace GAPSoft.Controllers
{
    public class EstadosComprasController : Controller
    {
        private GATSoftContext db = new GATSoftContext();

        // GET: EstadosCompras
        public ActionResult Index()
        {
            return View(db.EstadosCompras.ToList());
        }

        // GET: EstadosCompras/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            EstadosCompras estadosCompras = db.EstadosCompras.Find(id);
            if (estadosCompras == null)
            {
                return HttpNotFound();
            }
            return View(estadosCompras);
        }

        // GET: EstadosCompras/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: EstadosCompras/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "ID,Descripcion")] EstadosCompras estadosCompras)
        {
            if (ModelState.IsValid)
            {
                db.EstadosCompras.Add(estadosCompras);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(estadosCompras);
        }

        // GET: EstadosCompras/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            EstadosCompras estadosCompras = db.EstadosCompras.Find(id);
            if (estadosCompras == null)
            {
                return HttpNotFound();
            }
            return View(estadosCompras);
        }

        // POST: EstadosCompras/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "ID,Descripcion")] EstadosCompras estadosCompras)
        {
            if (ModelState.IsValid)
            {
                db.Entry(estadosCompras).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(estadosCompras);
        }

        // GET: EstadosCompras/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            EstadosCompras estadosCompras = db.EstadosCompras.Find(id);
            if (estadosCompras == null)
            {
                return HttpNotFound();
            }
            return View(estadosCompras);
        }

        // POST: EstadosCompras/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            EstadosCompras estadosCompras = db.EstadosCompras.Find(id);
            db.EstadosCompras.Remove(estadosCompras);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
