﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using GAPSoft.DAL;
using GAPSoft.Models;

namespace GAPSoft.Controllers
{
    public class EstadosProductosController : Controller
    {
        private GATSoftContext db = new GATSoftContext();

        // GET: EstadosProductos
        public ActionResult Index()
        {
            return View(db.EstadosProductos.ToList());
        }

        // GET: EstadosProductos/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            EstadosProductos estadosProductos = db.EstadosProductos.Find(id);
            if (estadosProductos == null)
            {
                return HttpNotFound();
            }
            return View(estadosProductos);
        }

        // GET: EstadosProductos/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: EstadosProductos/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "ID,Descripcion")] EstadosProductos estadosProductos)
        {
            if (ModelState.IsValid)
            {
                db.EstadosProductos.Add(estadosProductos);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(estadosProductos);
        }

        // GET: EstadosProductos/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            EstadosProductos estadosProductos = db.EstadosProductos.Find(id);
            if (estadosProductos == null)
            {
                return HttpNotFound();
            }
            return View(estadosProductos);
        }

        // POST: EstadosProductos/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "ID,Descripcion")] EstadosProductos estadosProductos)
        {
            if (ModelState.IsValid)
            {
                db.Entry(estadosProductos).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(estadosProductos);
        }

        // GET: EstadosProductos/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            EstadosProductos estadosProductos = db.EstadosProductos.Find(id);
            if (estadosProductos == null)
            {
                return HttpNotFound();
            }
            return View(estadosProductos);
        }

        // POST: EstadosProductos/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            EstadosProductos estadosProductos = db.EstadosProductos.Find(id);
            db.EstadosProductos.Remove(estadosProductos);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
