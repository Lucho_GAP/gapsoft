﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using GAPSoft.DAL;
using GAPSoft.Models;

namespace GAPSoft.Controllers
{
    public class TiposIvasController : Controller
    {
        private GATSoftContext db = new GATSoftContext();

        // GET: TiposIvas
        public ActionResult Index()
        {
            var tiposIva = db.TiposIva.Include(t => t.EstadoIva);
            return View(tiposIva.ToList());
        }

        // GET: TiposIvas/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            TiposIva tiposIva = db.TiposIva.Find(id);
            if (tiposIva == null)
            {
                return HttpNotFound();
            }
            return View(tiposIva);
        }

        // GET: TiposIvas/Create
        public ActionResult Create()
        {
            ViewBag.EstadosIvaID = new SelectList(db.EstadosIva, "ID", "Descripcion");
            return View();
        }

        // POST: TiposIvas/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "ID,Descripcion,Iva,EstadosIvaID")] TiposIva tiposIva)
        {
            if (ModelState.IsValid)
            {
                db.TiposIva.Add(tiposIva);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            ViewBag.EstadosIvaID = new SelectList(db.EstadosIva, "ID", "Descripcion", tiposIva.EstadosIvaID);
            return View(tiposIva);
        }

        // GET: TiposIvas/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            TiposIva tiposIva = db.TiposIva.Find(id);
            if (tiposIva == null)
            {
                return HttpNotFound();
            }
            ViewBag.EstadosIvaID = new SelectList(db.EstadosIva, "ID", "Descripcion", tiposIva.EstadosIvaID);
            return View(tiposIva);
        }

        // POST: TiposIvas/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "ID,Descripcion,Iva,EstadosIvaID")] TiposIva tiposIva)
        {
            if (ModelState.IsValid)
            {
                db.Entry(tiposIva).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.EstadosIvaID = new SelectList(db.EstadosIva, "ID", "Descripcion", tiposIva.EstadosIvaID);
            return View(tiposIva);
        }

        // GET: TiposIvas/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            TiposIva tiposIva = db.TiposIva.Find(id);
            if (tiposIva == null)
            {
                return HttpNotFound();
            }
            return View(tiposIva);
        }

        // POST: TiposIvas/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            TiposIva tiposIva = db.TiposIva.Find(id);
            db.TiposIva.Remove(tiposIva);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
