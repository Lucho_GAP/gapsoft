﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using GAPSoft.DAL;
using GAPSoft.Models;

namespace GAPSoft.Controllers
{
    public class EstadosPedidosController : Controller
    {
        private GATSoftContext db = new GATSoftContext();

        // GET: EstadosPedidos
        public ActionResult Index()
        {
            return View(db.EstadosPedidos.ToList());
        }

        // GET: EstadosPedidos/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            EstadosPedidos estadosPedidos = db.EstadosPedidos.Find(id);
            if (estadosPedidos == null)
            {
                return HttpNotFound();
            }
            return View(estadosPedidos);
        }

        // GET: EstadosPedidos/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: EstadosPedidos/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "ID,Descripcion")] EstadosPedidos estadosPedidos)
        {
            if (ModelState.IsValid)
            {
                db.EstadosPedidos.Add(estadosPedidos);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(estadosPedidos);
        }

        // GET: EstadosPedidos/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            EstadosPedidos estadosPedidos = db.EstadosPedidos.Find(id);
            if (estadosPedidos == null)
            {
                return HttpNotFound();
            }
            return View(estadosPedidos);
        }

        // POST: EstadosPedidos/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "ID,Descripcion")] EstadosPedidos estadosPedidos)
        {
            if (ModelState.IsValid)
            {
                db.Entry(estadosPedidos).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(estadosPedidos);
        }

        // GET: EstadosPedidos/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            EstadosPedidos estadosPedidos = db.EstadosPedidos.Find(id);
            if (estadosPedidos == null)
            {
                return HttpNotFound();
            }
            return View(estadosPedidos);
        }

        // POST: EstadosPedidos/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            EstadosPedidos estadosPedidos = db.EstadosPedidos.Find(id);
            db.EstadosPedidos.Remove(estadosPedidos);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
