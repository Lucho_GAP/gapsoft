﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using GAPSoft.DAL;
using GAPSoft.Models;

namespace GAPSoft.Controllers
{
    public class ProductosController : Controller
    {
        private GATSoftContext db = new GATSoftContext();

        // GET: Productos
        public ActionResult Index()
        {
            var productos = db.Productos.Include(p => p.EstadoPedido).Include(p => p.Grupo).Include(p => p.TipoIva);
            return View(productos.ToList());
        }

        // GET: Productos/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Productos productos = db.Productos.Find(id);
            if (productos == null)
            {
                return HttpNotFound();
            }
            return View(productos);
        }

        // GET: Productos/Create
        public ActionResult Create()
        {
            ViewBag.EstadosPedidosID = new SelectList(db.EstadosPedidos, "ID", "Descripcion");
            ViewBag.GruposProductosID = new SelectList(db.GruposProductos, "ID", "Descripcion");
            ViewBag.TiposIvaID = new SelectList(db.TiposIva, "ID", "Descripcion");
            return View();
        }

        // POST: Productos/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "ID,Descripcion,PrecioVenta,Stock,StockPedido,GruposProductosID,EstadosPedidosID,TiposIvaID")] Productos productos)
        {
            if (ModelState.IsValid)
            {
                db.Productos.Add(productos);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            ViewBag.EstadosPedidosID = new SelectList(db.EstadosPedidos, "ID", "Descripcion", productos.EstadosPedidosID);
            ViewBag.GruposProductosID = new SelectList(db.GruposProductos, "ID", "Descripcion", productos.GruposProductosID);
            ViewBag.TiposIvaID = new SelectList(db.TiposIva, "ID", "Descripcion", productos.TiposIvaID);
            return View(productos);
        }

        // GET: Productos/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Productos productos = db.Productos.Find(id);
            if (productos == null)
            {
                return HttpNotFound();
            }
            ViewBag.EstadosPedidosID = new SelectList(db.EstadosPedidos, "ID", "Descripcion", productos.EstadosPedidosID);
            ViewBag.GruposProductosID = new SelectList(db.GruposProductos, "ID", "Descripcion", productos.GruposProductosID);
            ViewBag.TiposIvaID = new SelectList(db.TiposIva, "ID", "Descripcion", productos.TiposIvaID);
            return View(productos);
        }

        // POST: Productos/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "ID,Descripcion,PrecioVenta,Stock,StockPedido,GruposProductosID,EstadosPedidosID,TiposIvaID")] Productos productos)
        {
            if (ModelState.IsValid)
            {
                db.Entry(productos).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.EstadosPedidosID = new SelectList(db.EstadosPedidos, "ID", "Descripcion", productos.EstadosPedidosID);
            ViewBag.GruposProductosID = new SelectList(db.GruposProductos, "ID", "Descripcion", productos.GruposProductosID);
            ViewBag.TiposIvaID = new SelectList(db.TiposIva, "ID", "Descripcion", productos.TiposIvaID);
            return View(productos);
        }

        // GET: Productos/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Productos productos = db.Productos.Find(id);
            if (productos == null)
            {
                return HttpNotFound();
            }
            return View(productos);
        }

        // POST: Productos/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Productos productos = db.Productos.Find(id);
            db.Productos.Remove(productos);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
