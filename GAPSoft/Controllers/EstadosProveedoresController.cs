﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using GAPSoft.DAL;
using GAPSoft.Models;

namespace GAPSoft.Controllers
{
    public class EstadosProveedoresController : Controller
    {
        private GATSoftContext db = new GATSoftContext();

        // GET: EstadosProveedores
        public ActionResult Index()
        {
            return View(db.EstadosClientes.ToList());
        }

        // GET: EstadosProveedores/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            EstadosClientes estadosProveedores = db.EstadosClientes.Find(id);
            if (estadosProveedores == null)
            {
                return HttpNotFound();
            }
            return View(estadosProveedores);
        }

        // GET: EstadosProveedores/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: EstadosProveedores/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "ID,Descripcion")] EstadosClientes EstadosClientes)
        {
            if (ModelState.IsValid)
            {
                db.EstadosClientes.Add(EstadosClientes);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(EstadosClientes);
        }

        // GET: EstadosProveedores/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            EstadosClientes estadosProveedores = db.EstadosClientes.Find(id);
            if (estadosProveedores == null)
            {
                return HttpNotFound();
            }
            return View(estadosProveedores);
        }

        // POST: EstadosProveedores/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "ID,Descripcion")] EstadosClientes EstadosClientes)
        {
            if (ModelState.IsValid)
            {
                db.Entry(EstadosClientes).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(EstadosClientes);
        }

        // GET: EstadosProveedores/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            EstadosClientes estadosProveedores = db.EstadosClientes.Find(id);
            if (estadosProveedores == null)
            {
                return HttpNotFound();
            }
            return View(estadosProveedores);
        }

        // POST: EstadosProveedores/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            EstadosClientes estadosProveedores = db.EstadosClientes.Find(id);
            db.EstadosClientes.Remove(estadosProveedores);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
