﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using GAPSoft.DAL;
using GAPSoft.Models;

namespace GAPSoft.Controllers
{
    public class ReglaFidelizacionsController : Controller
    {
        private GATSoftContext db = new GATSoftContext();

        // GET: ReglaFidelizacions
        public ActionResult Index()
        {
            var reglaFidelizacion = db.ReglaFidelizacion.Include(r => r.Producto);
            return View(reglaFidelizacion.ToList());
        }

        // GET: ReglaFidelizacions/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            ReglaFidelizacion reglaFidelizacion = db.ReglaFidelizacion.Find(id);
            if (reglaFidelizacion == null)
            {
                return HttpNotFound();
            }
            return View(reglaFidelizacion);
        }

        // GET: ReglaFidelizacions/Create
        public ActionResult Create()
        {
            ViewBag.ProductosID = new SelectList(db.Productos, "ID", "Descripcion");
            return View();
        }

        // POST: ReglaFidelizacions/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "ID,Cantidad,Puntos,ProductosID")] ReglaFidelizacion reglaFidelizacion)
        {
            if (ModelState.IsValid)
            {
                db.ReglaFidelizacion.Add(reglaFidelizacion);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            ViewBag.ProductosID = new SelectList(db.Productos, "ID", "Descripcion", reglaFidelizacion.ProductosID);
            return View(reglaFidelizacion);
        }

        // GET: ReglaFidelizacions/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            ReglaFidelizacion reglaFidelizacion = db.ReglaFidelizacion.Find(id);
            if (reglaFidelizacion == null)
            {
                return HttpNotFound();
            }
            ViewBag.ProductosID = new SelectList(db.Productos, "ID", "Descripcion", reglaFidelizacion.ProductosID);
            return View(reglaFidelizacion);
        }

        // POST: ReglaFidelizacions/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "ID,Cantidad,Puntos,ProductosID")] ReglaFidelizacion reglaFidelizacion)
        {
            if (ModelState.IsValid)
            {
                db.Entry(reglaFidelizacion).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.ProductosID = new SelectList(db.Productos, "ID", "Descripcion", reglaFidelizacion.ProductosID);
            return View(reglaFidelizacion);
        }

        // GET: ReglaFidelizacions/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            ReglaFidelizacion reglaFidelizacion = db.ReglaFidelizacion.Find(id);
            if (reglaFidelizacion == null)
            {
                return HttpNotFound();
            }
            return View(reglaFidelizacion);
        }

        // POST: ReglaFidelizacions/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            ReglaFidelizacion reglaFidelizacion = db.ReglaFidelizacion.Find(id);
            db.ReglaFidelizacion.Remove(reglaFidelizacion);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
