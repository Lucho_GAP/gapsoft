﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using GAPSoft.DAL;
using GAPSoft.Models;
using PagedList;

namespace GAPSoft.Controllers
{
    public class PedidosClientesController : Controller
    {
        private GATSoftContext db = new GATSoftContext();

        // GET: PedidosClientes
        public ActionResult Index(string BuscarNombre,string currentFilter, int? page)
        {
            if (BuscarNombre != null)
            {
                page = 1;
            }
            else
            {
                BuscarNombre = currentFilter;
            }

            ViewBag.CurrentFilter = BuscarNombre;
            
            
          var pedidosClientes = db.PedidosClientes.Include(p => p.Cliente);
            if(!string.IsNullOrEmpty(BuscarNombre)) 
            {
                //pedidosClientes = pedidosClientes.Where(p => p.Cliente.Nombres.Contains(BuscarNombre));
            }
            return View(pedidosClientes.ToList());
            //int pageSize = 5;
            //int pageNumber = (page ?? 1);
            //return View(pedidosClientes.ToPagedList(pageNumber, pageSize));
        }

        // GET: PedidosClientes/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            PedidosClientes pedidosClientes = db.PedidosClientes.Find(id);
            if (pedidosClientes == null)
            {
                return HttpNotFound();
            }
            return View(pedidosClientes);
        }

        // GET: PedidosClientes/Create
        public ActionResult Create()
        {
            ViewBag.ClientesID = new SelectList(db.Clientes, "ID", "Nombres");
            return View();
        }

        // POST: PedidosClientes/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "ID,Fecha,FechaEntrega,Abono,ValorTotal,ClientesID,EstadosPedidoID")] PedidosClientes pedidosClientes)
        {
            if (ModelState.IsValid)
            {
                db.PedidosClientes.Add(pedidosClientes);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            ViewBag.ClientesID = new SelectList(db.Clientes, "ID", "Nombres", pedidosClientes.ClientesID);
            return View(pedidosClientes);
        }

        // GET: PedidosClientes/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            PedidosClientes pedidosClientes = db.PedidosClientes.Find(id);
            if (pedidosClientes == null)
            {
                return HttpNotFound();
            }
            ViewBag.ClientesID = new SelectList(db.Clientes, "ID", "Nombres", pedidosClientes.ClientesID);
            return View(pedidosClientes);
        }

        // POST: PedidosClientes/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "ID,Fecha,FechaEntrega,Abono,ValorTotal,ClientesID,EstadosPedidoID")] PedidosClientes pedidosClientes)
        {
            if (ModelState.IsValid)
            {
                db.Entry(pedidosClientes).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.ClientesID = new SelectList(db.Clientes, "ID", "Nombres", pedidosClientes.ClientesID);
            return View(pedidosClientes);
        }

        // GET: PedidosClientes/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            PedidosClientes pedidosClientes = db.PedidosClientes.Find(id);
            if (pedidosClientes == null)
            {
                return HttpNotFound();
            }
            return View(pedidosClientes);
        }

        // POST: PedidosClientes/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            PedidosClientes pedidosClientes = db.PedidosClientes.Find(id);
            db.PedidosClientes.Remove(pedidosClientes);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
