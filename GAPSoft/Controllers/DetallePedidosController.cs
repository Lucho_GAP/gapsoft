﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using GAPSoft.DAL;
using GAPSoft.Models;

namespace GAPSoft.Controllers
{
    public class DetallePedidosController : Controller
    {
        private GATSoftContext db = new GATSoftContext();

        // GET: DetallePedidos
        public ActionResult Index()
        {
            var detallePedidos = db.DetallePedidos.Include(d => d.PedidoCliente).Include(d => d.Producto);
            return View(detallePedidos.ToList());
        }

        // GET: DetallePedidos/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            DetallePedidos detallePedidos = db.DetallePedidos.Find(id);
            if (detallePedidos == null)
            {
                return HttpNotFound();
            }
            return View(detallePedidos);
        }

        // GET: DetallePedidos/Create
        public ActionResult Create()
        {
            ViewBag.PedidosClientesID = new SelectList(db.PedidosClientes, "ID", "ID");
            ViewBag.ProductoSID = new SelectList(db.Productos, "ID", "Descripcion");
            return View();
        }

        // POST: DetallePedidos/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "ID,Descripcion,Cantidad,Valor,ValorTotal,ProductoSID,PedidosClientesID")] DetallePedidos detallePedidos)
        {
            if (ModelState.IsValid)
            {
                db.DetallePedidos.Add(detallePedidos);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            ViewBag.PedidosClientesID = new SelectList(db.PedidosClientes, "ID", "ID", detallePedidos.PedidosClientesID);
            ViewBag.ProductoSID = new SelectList(db.Productos, "ID", "Descripcion", detallePedidos.ProductoSID);
            return View(detallePedidos);
        }

        // GET: DetallePedidos/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            DetallePedidos detallePedidos = db.DetallePedidos.Find(id);
            if (detallePedidos == null)
            {
                return HttpNotFound();
            }
            ViewBag.PedidosClientesID = new SelectList(db.PedidosClientes, "ID", "ID", detallePedidos.PedidosClientesID);
            ViewBag.ProductoSID = new SelectList(db.Productos, "ID", "Descripcion", detallePedidos.ProductoSID);
            return View(detallePedidos);
        }

        // POST: DetallePedidos/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "ID,Descripcion,Cantidad,Valor,ValorTotal,ProductoSID,PedidosClientesID")] DetallePedidos detallePedidos)
        {
            if (ModelState.IsValid)
            {
                db.Entry(detallePedidos).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.PedidosClientesID = new SelectList(db.PedidosClientes, "ID", "ID", detallePedidos.PedidosClientesID);
            ViewBag.ProductoSID = new SelectList(db.Productos, "ID", "Descripcion", detallePedidos.ProductoSID);
            return View(detallePedidos);
        }

        // GET: DetallePedidos/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            DetallePedidos detallePedidos = db.DetallePedidos.Find(id);
            if (detallePedidos == null)
            {
                return HttpNotFound();
            }
            return View(detallePedidos);
        }

        // POST: DetallePedidos/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            DetallePedidos detallePedidos = db.DetallePedidos.Find(id);
            db.DetallePedidos.Remove(detallePedidos);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
