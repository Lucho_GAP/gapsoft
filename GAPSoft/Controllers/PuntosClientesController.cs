﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using GAPSoft.DAL;
using GAPSoft.Models;

namespace GAPSoft.Controllers
{
    public class PuntosClientesController : Controller
    {
        private GATSoftContext db = new GATSoftContext();

        // GET: PuntosClientes
        public ActionResult Index()
        {
            var puntosClientes = db.PuntosClientes.Include(p => p.Cliente);
            return View(puntosClientes.ToList());
        }

        // GET: PuntosClientes/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            PuntosClientes puntosClientes = db.PuntosClientes.Find(id);
            if (puntosClientes == null)
            {
                return HttpNotFound();
            }
            return View(puntosClientes);
        }

        // GET: PuntosClientes/Create
        public ActionResult Create()
        {
            ViewBag.ClientesID = new SelectList(db.Clientes, "ID", "Nombres");
            return View();
        }

        // POST: PuntosClientes/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "ID,PuntosDisponibles,PuntosTotales,PuntosRedimidos,ClientesID")] PuntosClientes puntosClientes)
        {
            if (ModelState.IsValid)
            {
                db.PuntosClientes.Add(puntosClientes);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            ViewBag.ClientesID = new SelectList(db.Clientes, "ID", "Nombres", puntosClientes.ClientesID);
            return View(puntosClientes);
        }

        // GET: PuntosClientes/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            PuntosClientes puntosClientes = db.PuntosClientes.Find(id);
            if (puntosClientes == null)
            {
                return HttpNotFound();
            }
            ViewBag.ClientesID = new SelectList(db.Clientes, "ID", "Nombres", puntosClientes.ClientesID);
            return View(puntosClientes);
        }

        // POST: PuntosClientes/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "ID,PuntosDisponibles,PuntosTotales,PuntosRedimidos,ClientesID")] PuntosClientes puntosClientes)
        {
            if (ModelState.IsValid)
            {
                db.Entry(puntosClientes).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.ClientesID = new SelectList(db.Clientes, "ID", "Nombres", puntosClientes.ClientesID);
            return View(puntosClientes);
        }

        // GET: PuntosClientes/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            PuntosClientes puntosClientes = db.PuntosClientes.Find(id);
            if (puntosClientes == null)
            {
                return HttpNotFound();
            }
            return View(puntosClientes);
        }

        // POST: PuntosClientes/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            PuntosClientes puntosClientes = db.PuntosClientes.Find(id);
            db.PuntosClientes.Remove(puntosClientes);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
