﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using GAPSoft.DAL;
using GAPSoft.Models;

namespace GAPSoft.Controllers
{
    public class DetalleFacturasController : Controller
    {
        private GATSoftContext db = new GATSoftContext();

        // GET: DetalleFacturas
        public ActionResult Index()
        {
            var detalleFacturas = db.DetalleFacturas.Include(d => d.Factura).Include(d => d.Producto);
            return View(detalleFacturas.ToList());
        }

        // GET: DetalleFacturas/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            DetalleFacturas detalleFacturas = db.DetalleFacturas.Find(id);
            if (detalleFacturas == null)
            {
                return HttpNotFound();
            }
            return View(detalleFacturas);
        }

        // GET: DetalleFacturas/Create
        public ActionResult Create()
        {
            ViewBag.FacturasID = new SelectList(db.Facturas, "ID", "ID");
            ViewBag.ProductosID = new SelectList(db.Productos, "ID", "Descripcion");
            return View();
        }

        // POST: DetalleFacturas/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "ID,Cantidad,Valor,ValorTotal,ProductosID,FacturasID")] DetalleFacturas detalleFacturas)
        {
            if (ModelState.IsValid)
            {
                db.DetalleFacturas.Add(detalleFacturas);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            ViewBag.FacturasID = new SelectList(db.Facturas, "ID", "ID", detalleFacturas.FacturasID);
            ViewBag.ProductosID = new SelectList(db.Productos, "ID", "Descripcion", detalleFacturas.ProductosID);
            return View(detalleFacturas);
        }

        // GET: DetalleFacturas/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            DetalleFacturas detalleFacturas = db.DetalleFacturas.Find(id);
            if (detalleFacturas == null)
            {
                return HttpNotFound();
            }
            ViewBag.FacturasID = new SelectList(db.Facturas, "ID", "ID", detalleFacturas.FacturasID);
            ViewBag.ProductosID = new SelectList(db.Productos, "ID", "Descripcion", detalleFacturas.ProductosID);
            return View(detalleFacturas);
        }

        // POST: DetalleFacturas/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "ID,Cantidad,Valor,ValorTotal,ProductosID,FacturasID")] DetalleFacturas detalleFacturas)
        {
            if (ModelState.IsValid)
            {
                db.Entry(detalleFacturas).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.FacturasID = new SelectList(db.Facturas, "ID", "ID", detalleFacturas.FacturasID);
            ViewBag.ProductosID = new SelectList(db.Productos, "ID", "Descripcion", detalleFacturas.ProductosID);
            return View(detalleFacturas);
        }

        // GET: DetalleFacturas/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            DetalleFacturas detalleFacturas = db.DetalleFacturas.Find(id);
            if (detalleFacturas == null)
            {
                return HttpNotFound();
            }
            return View(detalleFacturas);
        }

        // POST: DetalleFacturas/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            DetalleFacturas detalleFacturas = db.DetalleFacturas.Find(id);
            db.DetalleFacturas.Remove(detalleFacturas);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
