﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using GAPSoft.DAL;
using GAPSoft.Models;

namespace GAPSoft.Controllers
{
    public class DetallePuntosController : Controller
    {
        private GATSoftContext db = new GATSoftContext();

        // GET: DetallePuntos
        public ActionResult Index()
        {
            var detallePuntos = db.DetallePuntos.Include(d => d.PuntosCliente).Include(d => d.ReFidelizacion);
            return View(detallePuntos.ToList());
        }

        // GET: DetallePuntos/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            DetallePuntos detallePuntos = db.DetallePuntos.Find(id);
            if (detallePuntos == null)
            {
                return HttpNotFound();
            }
            return View(detallePuntos);
        }

        // GET: DetallePuntos/Create
        public ActionResult Create()
        {
            ViewBag.PuntosClientesID = new SelectList(db.PuntosClientes, "ID", "ID");
            ViewBag.ReglaFidelizacionID = new SelectList(db.ReglaFidelizacion, "ID", "ID");
            return View();
        }

        // POST: DetallePuntos/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "ID,Fecha,PuntosClientesID,ReglaFidelizacionID")] DetallePuntos detallePuntos)
        {
            if (ModelState.IsValid)
            {
                db.DetallePuntos.Add(detallePuntos);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            ViewBag.PuntosClientesID = new SelectList(db.PuntosClientes, "ID", "ID", detallePuntos.PuntosClientesID);
            ViewBag.ReglaFidelizacionID = new SelectList(db.ReglaFidelizacion, "ID", "ID", detallePuntos.ReglaFidelizacionID);
            return View(detallePuntos);
        }

        // GET: DetallePuntos/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            DetallePuntos detallePuntos = db.DetallePuntos.Find(id);
            if (detallePuntos == null)
            {
                return HttpNotFound();
            }
            ViewBag.PuntosClientesID = new SelectList(db.PuntosClientes, "ID", "ID", detallePuntos.PuntosClientesID);
            ViewBag.ReglaFidelizacionID = new SelectList(db.ReglaFidelizacion, "ID", "ID", detallePuntos.ReglaFidelizacionID);
            return View(detallePuntos);
        }

        // POST: DetallePuntos/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "ID,Fecha,PuntosClientesID,ReglaFidelizacionID")] DetallePuntos detallePuntos)
        {
            if (ModelState.IsValid)
            {
                db.Entry(detallePuntos).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.PuntosClientesID = new SelectList(db.PuntosClientes, "ID", "ID", detallePuntos.PuntosClientesID);
            ViewBag.ReglaFidelizacionID = new SelectList(db.ReglaFidelizacion, "ID", "ID", detallePuntos.ReglaFidelizacionID);
            return View(detallePuntos);
        }

        // GET: DetallePuntos/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            DetallePuntos detallePuntos = db.DetallePuntos.Find(id);
            if (detallePuntos == null)
            {
                return HttpNotFound();
            }
            return View(detallePuntos);
        }

        // POST: DetallePuntos/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            DetallePuntos detallePuntos = db.DetallePuntos.Find(id);
            db.DetallePuntos.Remove(detallePuntos);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
