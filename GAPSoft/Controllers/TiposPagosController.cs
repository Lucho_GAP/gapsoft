﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using GAPSoft.DAL;
using GAPSoft.Models;

namespace GAPSoft.Controllers
{
    public class TiposPagosController : Controller
    {
        private GATSoftContext db = new GATSoftContext();

        // GET: TiposPagos
        public ActionResult Index()
        {
            return View(db.TiposPagos.ToList());
        }

        // GET: TiposPagos/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            TiposPagos tiposPagos = db.TiposPagos.Find(id);
            if (tiposPagos == null)
            {
                return HttpNotFound();
            }
            return View(tiposPagos);
        }

        // GET: TiposPagos/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: TiposPagos/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "ID,Descripcion")] TiposPagos tiposPagos)
        {
            if (ModelState.IsValid)
            {
                db.TiposPagos.Add(tiposPagos);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(tiposPagos);
        }

        // GET: TiposPagos/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            TiposPagos tiposPagos = db.TiposPagos.Find(id);
            if (tiposPagos == null)
            {
                return HttpNotFound();
            }
            return View(tiposPagos);
        }

        // POST: TiposPagos/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "ID,Descripcion")] TiposPagos tiposPagos)
        {
            if (ModelState.IsValid)
            {
                db.Entry(tiposPagos).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(tiposPagos);
        }

        // GET: TiposPagos/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            TiposPagos tiposPagos = db.TiposPagos.Find(id);
            if (tiposPagos == null)
            {
                return HttpNotFound();
            }
            return View(tiposPagos);
        }

        // POST: TiposPagos/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            TiposPagos tiposPagos = db.TiposPagos.Find(id);
            db.TiposPagos.Remove(tiposPagos);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
